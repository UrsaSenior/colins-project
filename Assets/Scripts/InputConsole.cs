﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class InputConsole : MonoBehaviour
    {
        private static Camera _cachedCamera;

        private static Camera MainCamera { get { return _cachedCamera ?? (_cachedCamera = FindObjectOfType<Camera>()); } }

        public static Color BackgroundColour { set { MainCamera.backgroundColor = value; } }

        public static Color TextColour {
            set
            {
                _bufferedGuiInstance.SetColour(value);
                _inputGuiInstance.color = value;
            }
        }

        [Serializable]
        private class BufferedGui
        {
            // constants
            private const int MaxLines = 40;
            private readonly string[] IntroStrings;

            // variables
            [SerializeField] private Text _bufferUi;
            private List<string> _buffer = new List<string>(40);

            public BufferedGui()
            {
                string[] callback;
                Command.Parse(string.Empty, out callback);

                IntroStrings = callback.Concat(new [] { "Type a given command and press Enter to submit." }).ToArray();
            }
            
            public void Clear()
            {
                _bufferUi.text = string.Empty;
                _buffer.Clear();
                
                foreach (var line in IntroStrings)
                    _bufferUi.text += string.Format("{0}\n", line);
            }

            // write a line to the console string buffer
            public void AddLine(string newLine)
            {
                if (_buffer.Count == 0)
                    _buffer.Add(string.Empty);

                string[] callback;
                Command.Parse(newLine, out callback);

                // remove old newline
                _buffer.RemoveAt(_buffer.Count - 1);

                for (int i = 0; i < callback.Length; i++)
                    _buffer.Add(callback[i]);

                // add trailing newline to buffer 
                _buffer.Add(string.Empty);  

                // clear lines that are off-screen
                while (_buffer.Count > MaxLines)
                    _buffer.RemoveAt(0);

                var allText = new StringBuilder();

                // write the cached buffers to the stringbuilder
                foreach (var line in _buffer)
                {
                    allText.AppendLine(line);
                }

                // empty the stringbuilder into the gui text
                _bufferUi.text = allText.ToString();
            }

            public void SetColour(Color col) { _bufferUi.color = col; }
        }

        private static BufferedGui _bufferedGuiInstance;
        private static Text _inputGuiInstance;
        [SerializeField] private BufferedGui _console;
        [SerializeField] private Text _inputUi;

        private const string DefaultInputString = @"C:\> ";
        private static readonly int DefaultInputLength = DefaultInputString.Length;


        private void Awake()
        {
            _inputUi.text = DefaultInputString;
            _bufferedGuiInstance = _console;
            _inputGuiInstance = _inputUi;
        }

        private void Start()
        {
            _console.Clear();
        }

        private void Update()
        {
            if (!Input.anyKeyDown) return;

            UpdateInput();
        }

        private void UpdateInput()
        {
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                var len = _inputUi.text.Length;

                // prevent deleting default input tokens
                if (len == DefaultInputLength) return;

                if (len > 0)
                    _inputUi.text = _inputUi.text.Remove(len - 1, 1);
                return;
            }

            if (IsInputNewline())
            {
                // remove trailing newline from buffer
                // remove top line from buffer (if oversize)
                // add new input to buffer
                _console.AddLine(_inputUi.text.Substring(DefaultInputLength));

                // clear input buffer
                _inputUi.text = DefaultInputString;

                return;
            }
            
            // inputstring will only provide ascii
            _inputUi.text = _inputUi.text + Input.inputString;
        }
        
        private static bool IsInputNewline()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.LeftShift)) return true;
#endif

            return Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter);
        }
    }
}
