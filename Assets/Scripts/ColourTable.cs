﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public static class ColourTable
    {
        public static readonly Dictionary<string, Color> CachedColors = new Dictionary<string, Color>
        {
            { "red", Color.red },
            { "blue", Color.blue },
            { "green", Color.green },
            { "yellow", Color.yellow },
            { "cyan", Color.cyan },
            { "magenta", Color.magenta },
            { "gold", new Color(1f, 215f / 256f, 0f) },
            { "aqua", Color.cyan },
            { "pink", Color.magenta },
            { "black", Color.black },
            { "white", Color.white },
            { "grey", Color.gray },
            { "purple" , new Color(0.5f, 0f, 0.5f)},
            { "orange", new Color(1f, 165f/255f, 0f) }
        };
    }
}