﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using UnityEngine;

public class Boids : MonoBehaviour
{
    private static Rect _boundaries;
    public static Rect Boundaries
    {
        get
        {
            if (_boundaries == default(Rect))
            {
                var camera = FindObjectOfType<Camera>();
                var bottomLeft = camera.ViewportToWorldPoint(new Vector3(0f, 0f, 1f));
                var topRight = camera.ViewportToWorldPoint(new Vector3(1f, 1f, 1f));
                var width = Mathf.Abs(bottomLeft.x) + Mathf.Abs(topRight.x);
                var height = Mathf.Abs(bottomLeft.y) + Mathf.Abs(topRight.y);
                _boundaries = new Rect(bottomLeft.x, bottomLeft.y, width, height);
            }
            return _boundaries;
        }
    }

    private static GameObject protoBoid;

    private static GameObject GetNewAgent()
    {
        if (protoBoid == null)
        {
            var obj = new GameObject("agent", typeof(MeshFilter), typeof(MeshRenderer));
            var meshComponent = obj.GetComponent<MeshFilter>();
            var mesh = meshComponent.mesh;
            mesh.vertices = new[]
            {
                new Vector3(-0.05f, -0.05f, 0.05f),
                new Vector3(0f, 0.05f, 0.05f),
                new Vector3(0.05f, -0.05f, 0.05f),
            };
            mesh.triangles = new[]
            {
                0, 1, 2
            };
            mesh.normals = new[]
            {
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward,
            };
            mesh.uv = new[]
            {
                new Vector2(-1f, -1f),
                new Vector2(0f, 1f),
                new Vector2(1f, -1f),
            };
            obj.SetActive(false);
            obj.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
            protoBoid = obj;
        }
        var newObj = GameObject.Instantiate(protoBoid);
        newObj.SetActive(true);
        newObj.AddComponent<Agent>();
        return newObj;
    }
    
    public static string[] Add(params string[] args)
    {
        var agent = GetNewAgent();

        var callbackLines = Parse(agent.GetComponent<Agent>(), args);

        if (callbackLines.Length > 0)
            callbackLines[0] = string.Format("Adding {0}agent.", callbackLines[0]);
        else
            callbackLines = new[] {"Adding agent."};

        return callbackLines;
    }

    private static string[] Parse(Agent agent, params string[] args)
    {
        // reset args to unused state
        foreach (var arg in AllAgentArgs) arg.HasAlreadyBeenUsed = false;

        StringBuilder validArgs = new StringBuilder(args.Length);
        List<string> invalidArgs = new List<string>(args);
        
        foreach (var arg in args)
            foreach (var agentArg in AllAgentArgs)
            {
                if (agentArg.TryParse(agent, arg))
                {
                    invalidArgs.Remove(arg);
                    validArgs.Append(arg);
                    validArgs.Append(" ");
                }
            }

        if (invalidArgs.Count == 0)
            return new[] { validArgs.ToString() };

        return new[]
        {
            validArgs.ToString(),
            string.Format("Dropped commands: {0}", string.Join(" ", invalidArgs.Select(arg => arg).ToArray())),
        };
    }

    private static readonly List<AgentArg> AllAgentArgs = new List<AgentArg>
    {
        new CloneAgentArg(),
        SizeAgentArg.Instance,
        new ColourAgentArg(),
    };

    private class AgentArg
    {
        // todo reset to false before use
        public bool HasAlreadyBeenUsed;
        private readonly Func<string, bool> _isArgValidEvent;
        private readonly Action<Agent> _onValidEvent;

        public AgentArg(Func<string, bool> isArgValid, Action<Agent> agentEffect)
        {
            _isArgValidEvent = isArgValid;
            _onValidEvent = agentEffect;
        }
        
        public bool TryParse(Agent agent, string arg)
        {
            var isValid = !HasAlreadyBeenUsed && _isArgValidEvent(arg);
            if (!isValid) return false;

            HasAlreadyBeenUsed = true;
            _onValidEvent(agent);
            return true;
        }
    }

    private class CloneAgentArg : AgentArg
    {

        public CloneAgentArg() 
            : base(IsArgValid, ApplyClone)
        {
        }

        private static int _numToClone = 0;

        private static bool IsArgValid(string input)
        {
            return int.TryParse(input, out _numToClone) &&
                _numToClone > 0 && Agent.Count < Agent.MaxAgents;
        }

        private static void ApplyClone(Agent agent)
        {
            agent.Clone = _numToClone - 1;
        }
    }

    private class ColourAgentArg : AgentArg
    {
        private static Color _cachedColor;

        public ColourAgentArg() : base(IsArgValid, ApplyColour) { }

        private static bool IsArgValid(string input)
        {
            return ColourTable.CachedColors.ContainsKey(input) && 
                ColourTable.CachedColors.TryGetValue(input, out _cachedColor);
        }

        private static void ApplyColour(Agent agent)
        {
            agent.SetColor = _cachedColor;
        }
    }

    private class SizeAgentArg : AgentArg
    {
        private static SizeAgentArg _instance;

        public static SizeAgentArg Instance
        {
            get { return _instance ?? (_instance = new SizeAgentArg(IsArgValid, SizeAgentEffect)); }
        }

        private SizeAgentArg(Func<string, bool> a, Action<Agent> b) : base(a, b) { }

        // pairing of size name to size value
        private static readonly Dictionary<string, float> SizeNames = new Dictionary<string, float>
        {
            { "small", 0.75f },
            { "little", 0.5f },
            { "mini", 0.25f },
            { "tiny", 0.125f },
            { "large", 1.25f },
            { "big" , 1.5f },
            { "giant", 1.75f },
            { "huge", 2f },
            { "massive", 4f },
            { "humungous", 6f },
            { "leviathan", 8f },
            { "behemoth", 10f },
        };

        private static float _applySize = 1f;

        private static bool IsArgValid(string input)
        {
            return SizeNames.TryGetValue(input, out _applySize);
        }

        private static void SizeAgentEffect(Agent agent)
        {
            agent.SetSize = _applySize;
        }
    }
}
