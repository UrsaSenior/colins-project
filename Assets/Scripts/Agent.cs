﻿using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour
{
    public const int MaxAgents = 100;

    [SerializeField] private Vector2 _position;
    [SerializeField] private Vector2 _velocity;
    [SerializeField] private Vector2 _acceleration;
    public static float MaxSpeed = .03f;
    public static float MaxForce = 0.015f;
    public static float SeperationDist = .25f;
    public static float NeighborDist = 0.5f; // todo clamp above seperation
    public static float SeperationForce = 1f;
    public static float AlignmentForce = 0.125f;
    public static float CohesionForce = 0.25f;

    private Material _material;

    private static Shader _cachedShader;

    private static HashSet<Agent> _swarmAgents = new HashSet<Agent>();

    public static void Default()
    {
        MaxSpeed = .03f;
        MaxForce = 0.015f;
        SeperationDist = .25f;
        NeighborDist = 0.5f;
        SeperationForce = 1f;
        AlignmentForce = 0.125f;
        CohesionForce = 0.25f;
    }

    public static void DestroyAll()
    {
        foreach (var agent in _swarmAgents)
            GameObject.Destroy(agent.gameObject);
        _swarmAgents.Clear();
    }

    public static int Count { get { return _swarmAgents != null ? _swarmAgents.Count : 0; } }

    private static Shader RawColour
    {
        get { return _cachedShader ?? (_cachedShader = Shader.Find("RawColour")); }
    }
    
    // todo mass
    public float SetSize
    {
        set { transform.localScale = Vector3.one * value; }
        get { return transform.localScale.x; }
    }

    public Color SetColor
    {
        set { _material.SetColor("_Color", value); }
        private get { return _material.GetColor("_Color"); }
    }

    public int Clone { set; private get; }
    private int _cloneNextUpdate;

    private void LateUpdate()
    {
        while (_cloneNextUpdate-- > 0)
        {
            var a = GameObject.Instantiate(gameObject).GetComponent<Agent>();
            a.SetColor = SetColor;
            a.SetSize = SetSize;
        }

        if (Clone < 1) return;

        _cloneNextUpdate = Clone;
        Clone = 0;
    }

    private void Awake()
    {
        _swarmAgents.Add(this);
        
        if (_velocity == Vector2.zero)
            _velocity = new Vector2(Random.Range(-0.7f, 0.7f), Random.Range(-0.7f, 0.7f));
        
        _material = GetComponent<Renderer>().sharedMaterial = new Material(RawColour);
        if (SetColor == Color.clear)
        {
            var randColour = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            SetColor = randColour;
        }
    }

    private void Destroy()
    {
        _swarmAgents.Remove(this);
    }
    
    // Method to update position
    private void Update()
    {
        // apply boundaries
        _acceleration = GetAccelerateToAvoidBoundaries();

        //apply flocking behaviour
        _acceleration += GetForceToSeperateFlock() * SeperationForce;
        _acceleration += GetForceToAlignFlock() * AlignmentForce;
        _acceleration += GetForceToCohereFlock() * CohesionForce;
        //_acceleration += GetForceToSteerToPosition(Vector2.one);

        // apply basic motion
        _velocity += _acceleration;
        _velocity = _velocity.normalized * MaxSpeed;
        _position += _velocity;

        // apply transform
        transform.position = _position;

        // apply rotation 
        var angle = Mathf.Atan2(_velocity.x, _velocity.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);
    }

    // Seperation - steer away from nearby agents
    private Vector2 GetForceToSeperateFlock()
    {
        Vector2 steer = Vector2.zero;
        int localSwarmSize = 0;

        foreach (var agent in _swarmAgents)
        {
            if (agent == this) continue;

            float distance = Vector2.Distance(_position, agent._position);

            if (distance > 0f && distance < SeperationDist)
            {
                Vector2 diff = (_position - agent._position).normalized;
                diff /= distance;
                steer += diff;
                ++localSwarmSize;
            }
        }

        // average the accumulated steering force
        if (localSwarmSize != 0) steer /= (float)localSwarmSize;

        if (steer.sqrMagnitude > 0f)
            steer = ClampMaxForce((steer.normalized * MaxSpeed) - _velocity);

        return steer;
    }

    // Alignment - find the average velocity for nearby agents in swarm
    private Vector2 GetForceToAlignFlock()
    {
        Vector2 sum = Vector2.zero;
        int localSwarmSize = 0;

        foreach (var agent in _swarmAgents)
        {
            if (agent == this) continue;
            float distance = Vector2.Distance(_position, agent._position);
            if (distance > 0f && distance < NeighborDist)
            {
                sum += agent._velocity;
                ++localSwarmSize;
            }
        }

        if (localSwarmSize != 0)
        {
            sum = (sum / (float) localSwarmSize).normalized * MaxSpeed;
            
            return ClampMaxForce(sum - _velocity);
        }

        return Vector2.zero;
    }

    // Cohesion - steer towards the average position of each nearby agent in swarm
    private Vector2 GetForceToCohereFlock()
    {
        Vector2 sum = Vector2.zero;
        int localSwarmSize = 0;

        foreach (var agent in _swarmAgents)
        {
            float distance = Vector2.Distance(_position, agent._position);

            if (distance > 0f && distance < NeighborDist)
            {
                ++localSwarmSize;
                sum += agent._position;
            }
        }

        if (localSwarmSize != 0)
        {
            sum /= localSwarmSize;
            return GetForceToSteerToPosition(sum);
        }

        return Vector2.zero;
    }

    private Vector2 GetForceToSteerToPosition(Vector2 target)
    {
        Vector2 desired = (target - _position).normalized * MaxSpeed;
        return ClampMaxForce(desired - _velocity);
    }

    private Vector2 GetAccelerateToAvoidBoundaries()
    {
        Vector2 desired = Vector2.zero;

        if (_position.x < Boids.Boundaries.x)
        {
            desired = new Vector2(MaxSpeed, _velocity.y);
        }
        else if (_position.x > Boids.Boundaries.x + Boids.Boundaries.width)
        {
            desired = new Vector2(-MaxSpeed, _velocity.y);
        }

        if (_position.y < Boids.Boundaries.y)
        {
            desired = new Vector2(_velocity.x, MaxSpeed);
        }
        else if (_position.y > Boids.Boundaries.y + Boids.Boundaries.height)
        {
            desired = new Vector2(_velocity.x, -MaxSpeed);
        }

        if (desired != Vector2.zero)
        {
            desired = ClampMaxForce((desired.normalized * MaxSpeed) - _velocity);
            // we could add mass here if we want A = F / M
        }
        return desired;
    }

    private Vector2 ClampMaxForce(Vector2 input)
    {
        if (input.magnitude > MaxForce)
            input = input.normalized * MaxForce;
        return input;
    }
}
