﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public static class Command
    {
        private const float ScalingFactor = 100f;

        public static void Parse(string input, out string[] callback)
        {
            if (input.Length == 0)
            {
                callback = new[] {string.Format("Valid inputs: {0}", AllValidCommandsByName)};
                return;
            }

            // split string into segments
            string[] parameters = input.Split(' ');

            var cmdName = parameters[0].ToLowerInvariant();

            if (AllCommands.ContainsKey(cmdName))
            {
                callback = AllCommands[cmdName].Execute(parameters.Skip(1).ToArray());
                return;
            }
            callback = new[]
            {
                string.Format("'{0}' is not a valid command!", parameters[0]),
                string.Format("Valid inputs: {0}", AllValidCommandsByName),
            };
        }

        private const float 
            MaxSpeed = 0.1f, MinSpeed = 0.01f, DefaultSpeed = 0.03f,
            MaxForce = 0.1f, MinForce = 0.001f, DefaultForce = 0.015f,
            MaxSpace = 1f, MinSpace = 0f, DefaultSpace = 0.25f,
            MaxGroup = 1f, MinGroup = 0f, DefaultGroup = 0.25f,
            MaxRange = 2f, MinRange = 0f, DefaultRange = 0.5f,
            MaxDir = 1f, MinDir = 0f, DefaultDir = 0.125f,
            MaxAvoid = 1.5f, MinAvoid = 0f, DefaultAvoid = 1f;

        private static readonly Dictionary<string, Cmd> AllCommands = new Dictionary<string, Cmd>
        {
            { "stats", new PrintStatsCmd() },
            { "add", new AddAgentCmd() },
            { "remove", new RemoveAgentCmd() },
            { "reset",  new ResetCmd()},
            { "speed", new SetAgentCmd("MaxSpeed", DefaultSpeed, MinSpeed, MaxSpeed) },
            { "force", new SetAgentCmd("MaxForce", DefaultForce, MinForce, MaxForce) },
            { "space", new SetAgentCmd("SeperationDist", DefaultSpace, MinSpace, MaxSpace) },
            { "group", new SetAgentCmd("CohesionForce", DefaultGroup, MinGroup, MaxGroup) },
            { "range", new SetAgentCmd("NeighborDist", DefaultRange, MinRange, MaxRange) },
            { "direction", new SetAgentCmd("AlignmentForce", DefaultDir, MinDir, MaxDir) },
            { "avoid", new SetAgentCmd("SeperationForce", DefaultAvoid, MinAvoid, MaxAvoid)},
            { "background", new SetColourCmd(input => InputConsole.BackgroundColour = input, "background") },
            { "text", new SetColourCmd(input => InputConsole.TextColour = input, "text") },
            { "exit", QuitCmd.Instance },
        };

        private static readonly string AllValidCommandsByName = string.Join(", ", AllCommands.Select(cmds => cmds.Key).ToArray());

        private abstract class Cmd
        {
            public abstract string[] Execute(params string[] args);
            
            public struct CmdArg
            {
                private bool _hasBeenValidatedOnce;
                private Func<string, bool> _isInputValidEvent;

                public CmdArg(Func<string, bool> isInputValid)
                {
                    _hasBeenValidatedOnce = false;
                    _isInputValidEvent = isInputValid ?? DefaultEvent;
                }

                private static bool DefaultEvent(string _) { return false; }

                public bool Parse(string input)
                {
                    return !_hasBeenValidatedOnce && _isInputValidEvent(input);
                }
            }
        }

        private class PrintStatsCmd : Cmd
        {
            public override string[] Execute(params string[] args)
            {
                return new []
                {
                    string.Format("Total {0} agents active: Speed {1:F1}, Force {2:F1}, Spacing {3:F1}, Grouping {4:F1}, Range {5:F1}, Direction {6:F1}, Avoidance {7:F1}",
                        Agent.Count, 
                        Agent.MaxSpeed / ((MaxSpeed - MinSpeed)/ ScalingFactor),
                        Agent.MaxForce / ((MaxForce - MinForce)/ ScalingFactor),
                        Agent.SeperationDist / ((MaxSpace - MinSpace)/ ScalingFactor),
                        Agent.CohesionForce / ((MaxGroup - MinGroup)/ ScalingFactor),
                        Agent.NeighborDist / ((MaxRange - MinRange)/ ScalingFactor),
                        Agent.AlignmentForce / ((MaxDir - MinDir)/ ScalingFactor),
                        Agent.SeperationForce / ((MaxAvoid - MinAvoid)/ ScalingFactor))
                };
            }
        }

        private class RemoveAgentCmd : Cmd
        {
            public override string[] Execute(params string[] args)
            {
                var count = Agent.Count;
                Agent.DestroyAll();
                return new[] {string.Format("Removed {0} agents.", count)};
            }
        }

        private class ResetCmd : Cmd
        {
            public override string[] Execute(params string[] args)
            {
                var count = Agent.Count;
                Agent.Default();
                Agent.DestroyAll();
                return new[] { string.Format("Removed {0} agents and reset properties.", count) };
            }
        }

        private class QuitCmd : Cmd
        {
            public static readonly QuitCmd Instance = new QuitCmd();

            public override string[] Execute(params string[] args)
            {
                CoroutineObject.Start(ShutdownAfterDelay(1.5f));
                return new[] {"Shutting down. Thank you for playing!"};
            }

            private static IEnumerator ShutdownAfterDelay(float waitTime)
            {
                yield return new WaitForSecondsRealtime(waitTime);

                Application.Quit();
            }
        }
        
        private class AddAgentCmd : Cmd
        {
            public override string[] Execute(params string[] args)
            {
                if (args.Length == 0)
                    return Boids.Add(args).Concat(new string[]
                    {
                        "Adds a flocking agent into the game. Use descriptive words to change the size & colour.",
                        "examples: add 5, add pink, add huge, add 3 pink tiny"
                    }).ToArray();

                return Boids.Add(args);
            }
        }

        private class SetAgentCmd : Cmd
        {
            private string _propertyName;
            private Action<float> _setPropertyEvent;

            private float _default;
            private float _scale; // 100 / (_max - min)

            public SetAgentCmd(string propertyName, float @default, 
                float min = 0f, float max = 1f)
            {
                _scale = (max - min)/ ScalingFactor;
                _default = @default;
                _propertyName = propertyName;
                var property = typeof(Agent).GetField(propertyName);
                _setPropertyEvent = value => property.SetValue(null, value);
            }

            public override string[] Execute(params string[] args)
            {
                float scaledValue;
                foreach (var arg in args)
                {
                    if (Single.TryParse(arg, out scaledValue))
                    {
                        var unscaledValue = scaledValue*_scale;
                        if (unscaledValue > 0f && unscaledValue < 100f)
                        {
                            _setPropertyEvent(unscaledValue);
                            return new [] { string.Format("{0} set to {1}.", _propertyName, scaledValue)};
                        }
                    }
                }

                return new []
                {
                    string.Format("Please add a number 1 to {1} (default is {0:F1}) to the command.",
                        _default / _scale, ScalingFactor)
                };
            }
        }

        private class SetColourCmd : Cmd
        {
            private readonly Action<Color> _setColourEvent;
            private readonly string _targetName;

            public SetColourCmd(Action<Color> setColourTarget, string targetName)
            {
                _targetName = targetName;
                _setColourEvent = setColourTarget;
            }

            public override string[] Execute(params string[] args)
            {
                if (args.Length == 0)
                    return new [] { string.Format("Sets the {0} colour.\texample: {0} pink", _targetName)};

                foreach (var arg in args)
                {
                    Color newColor;
                    if (ColourTable.CachedColors.TryGetValue(arg, out newColor))
                    {
                        _setColourEvent(newColor);
                        return new[] {string.Format("Set {0} to {1}.", _targetName, arg)};
                    }
                }
                
                return new[]
                {
                    string.Format("Cannot set {0} colour, unrecognised options: {1}",
                        _targetName, string.Join(" ", args.Select(value => value).ToArray()))
                };
            }
        }
    }
}