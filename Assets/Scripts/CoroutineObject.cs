﻿using UnityEngine;
using System.Collections;

public class CoroutineObject : MonoBehaviour
{
    private static CoroutineObject _instance;

    private static CoroutineObject instance
    {
        get {  return _instance ?? (_instance = new GameObject("_CoroutineObject").AddComponent<CoroutineObject>()); }
    }

    public static void Start(IEnumerator coroutine)
    {
        instance.StartCoroutine(coroutine);
    }

    private void OnApplicationQuit()
    {
        StopAllCoroutines();
    }
}
